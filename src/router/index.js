import { createWebHistory, createRouter } from "vue-router";
import Main from "../components/main/Main.vue";
import Auth from "../components/user/Auth.vue";
import Settings from '../components/settings/Settings.vue'
import NotFound from "../components/404.vue";

const routes = [
  {
    path: "/",
    name: "Main",
    component: Main,
    meta: { requiresAuth: true }
  },
  {
    path: "/login",
    name: "Auth",
    component: Auth,
    meta: { requiresAuth: false }
  },
  {
    path: "/settings",
    name: "Settings",
    component: Settings,
    meta: { requiresAuth: true }
  },
  {
    path: "/404",
    name: "NotFound",
    component: NotFound,
    meta: { requiresAuth: false }
  },
];

const router = createRouter({
    mode: 'history',
    history: createWebHistory(),
    routes,
});

// authentication
router.beforeEach((to, from, next) => {
    const requiresAuth = to.matched.some(record => record.meta.requiresAuth);
    const isAuthenticated = localStorage.getItem('auth');

    console.log('path', to.path);
    console.log('requiresAuth', requiresAuth);
    console.log('isAuthenticated', isAuthenticated);

    if (to.path == '/login' && isAuthenticated) {
        next("/");
    }

    if (requiresAuth && !isAuthenticated) {
        next("/login");
    } else {
        next();
    }
});

export default router;
