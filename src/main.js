import { createApp } from 'vue';
import App from './App.vue';
import mitt from 'mitt';
import router from './router';
import Vuex from 'vuex'

const emitter = mitt();
const app = createApp(App).use(router).use(Vuex);

// bootstrap
import "bootstrap/dist/css/bootstrap.min.css"
import "bootstrap"

app.config.globalProperties.emitter = emitter;
app.mount('#app');
